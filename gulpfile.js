/**
 * https://www.liquidlight.co.uk/blog/how-do-i-update-to-gulp-4/
 * */

'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('gulp4-run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var stripCssComments = require('gulp-strip-css-comments');
var concatCss = require('gulp-concat-css');
var replace = require('gulp-replace');
var fs = require('fs');
var child_process = require('child_process');

var path = process.argv.pop();
if ( path.indexOf('--') !== 0 ) {
	console.log("Path for site not defined. Please define it as parameter with:\ngulp --site.ext");
	process.exit(-1);
}
path = '/var/www/' + path.replace('--','');

// Set the browser that you want to supoprt
const AUTOPREFIXER_BROWSERS = [
	'ie >= 10',
	'ie_mob >= 10',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4.4',
	'bb >= 10'
];

// Gulp task to minify CSS files
gulp.task('styles', function () {
	return gulp.src([path + '/src/style.css'])
	// Auto-prefix css styles for cross browser compatibility
	//    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
		.pipe(stripCssComments({preserve: false}))
	//	.pipe(concatCss("style.css")) // this shitty script remove ' from background-image: url("data:image/svg+xml,%3Csvg xmlns='
	// Minify the file
		.pipe(csso())
	// Output
		.pipe(gulp.dest('./build'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
	return gulp.src(path + '/src/*.js')
	// Minify the file
		.pipe(uglify())
	// Output
		.pipe(gulp.dest('./build'))
});

// Gulp task to minify HTML files
gulp.task('pages', function() {
	return gulp.src([path + '/src/*.html'])
		.pipe(htmlmin({
			collapseWhitespace: true,
			removeComments: true
		}))
		.pipe(gulp.dest('./build'));
});

// Gulp task to inline css and js to HTML files
gulp.task('inline', function() {
	return gulp.src(['build/*.html'])
		.pipe(replace(/<link rel="stylesheet" href="style.css">/, function(s) {
			var style = fs.readFileSync('build/style.css', 'utf8');
			return '<style>' + style + '</style>';
		}))
		.pipe(replace(/<script type="text\/javascript" src="main.js">/, function(s) {
			var script = fs.readFileSync('build/main.js', 'utf8');
			return '<script>' + script;
		}))
		.pipe(replace(/<\/script>/, function(s) {
			var script = fs.readFileSync(path + '/src/yandex.tpl', 'utf8');
			return '</script>' + script;
		}))
		.pipe(gulp.dest(path + '/public_html'));
});

gulp.task('deploy', function() {
	return	child_process.exec( path + '/deploy.sh' );
});

// Clean output directory
gulp.task('clean', () => del(['./build']));

// Gulp task to minify all files
gulp.task('default', gulp.series('clean', function (done) {
	runSequence(
		'styles',
		'scripts',
		'pages',
		'inline',
		'deploy'
	);
	done();
}));
